$(document).ready(function() {
    var preguntaId = $("form").data("id");

    var reloj = $("#temporizador");
    var ahora = new Date();
    var tiempoLimite = new Date(ahora.getFullYear(), ahora.getMonth(), ahora.getDate(), ahora.getHours(), ahora.getMinutes(), ahora.getSeconds() + 20);

    countdown.resetLabels();
    countdown.setLabels('| segundo', '| segundos');

    $(reloj).html(countdown(tiempoLimite).toString());
    setInterval(function(){
        $(reloj).html(countdown(tiempoLimite).toString());
        if (!$(reloj).html()) {
            window.location.reload();
        }
    }, 1000);
});
