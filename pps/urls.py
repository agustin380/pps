from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views

import validacion.urls
import backoffice.urls

urlpatterns = [
    url(r'^validacion/', include(validacion.urls)),
    url(r'^backoffice/', include(backoffice.urls)),
    url(r'^login/$', auth_views.login, {'template_name': 'validacion/login.html'}, name="login"),
    url(r'^logout/$', auth_views.logout, {'next_page': "/validacion/"}, name="logout"),
    url(r'^admin/', include(admin.site.urls)),
]
