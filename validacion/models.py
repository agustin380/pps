from random import shuffle

from django.db import models
from django.conf import settings


class Perfil(models.Model):
    usuario = models.OneToOneField(settings.AUTH_USER_MODEL)
    dni = models.CharField(max_length=8)
    fecha_nacimiento = models.DateField()
    telefono = models.CharField(max_length=10)


class Validacion(models.Model):
    RECHAZADA = "r"
    APROBADA = "a"
    MANUAL = "m"
    EN_CURSO = "e"

    ESTADOS = (
        (RECHAZADA, "Rechazada"),
        (APROBADA, "Aprobada"),
        (MANUAL, "Manual"),
        (EN_CURSO, "En curso"),
    )
    estado = models.CharField(max_length=1, choices=ESTADOS, default=EN_CURSO)
    usuario = models.OneToOneField(Perfil, null=True, blank=True)

    def calcular_resultado(self):
        correctas = self.pregunta_set.filter(estado=Pregunta.CORRECTA).count()
        if correctas == 3:
            self.estado = Validacion.APROBADA
        elif correctas == 2:
            self.estado = Validacion.MANUAL
        else:
            self.estado = Validacion.RECHAZADA
        self.save()


class Pregunta(models.Model):
    SIN_RESPONDER = "s"
    CORRECTA = "c"
    INCORRECTA = "i"

    ESTADOS = (
        (SIN_RESPONDER, "Sin responder"),
        (CORRECTA, "Correcta"),
        (INCORRECTA, "Incorrecta"),
    )

    DOMICILIO = "d"
    EMPRESA = "e"
    VEHICULO = "v"

    TIPOS = (
        (DOMICILIO, "¿Reconocés alguno de estos domicilios, que pueden ser actuales o antiguos?"),
        (EMPRESA, "¿Trabajás o trabajaste para alguna de las siguientes empresas?"),
        (VEHICULO, "¿Tenés o has tenido en el pasado alguno de estos vehículos (automóvil / motocicleta)?"),
    )

    OPCION_NINGUNA = "Ninguna de las anteriores"

    respuesta = models.CharField(max_length=40, default=OPCION_NINGUNA)
    tipo = models.CharField(max_length=1, choices=TIPOS)
    estado = models.CharField(max_length=1, choices=ESTADOS, default=SIN_RESPONDER)
    visto = models.BooleanField(default=False)
    validacion = models.ForeignKey(Validacion, null=True)

    def construir_opciones(self):
        # Traer preguntas existentes del mismo tipo
        respuestas = Pregunta.objects.filter(tipo=self.tipo).exclude(validacion=self.validacion).order_by("?")
        opciones = list(respuestas[:3]) + [self]
        shuffle(opciones)
        return opciones

    def correcta(self):
        self.estado = Pregunta.CORRECTA
        return self.save()

    def incorrecta(self):
        self.estado = Pregunta.INCORRECTA
        return self.save()

    def __str__(self):
        return self.respuesta
