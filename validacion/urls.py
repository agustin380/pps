from django.conf.urls import url
import validacion.views as views

urlpatterns = [
    url(r'^$', views.home, name="home"),
    url(r'^preguntas/$', views.preguntas, name="preguntas"),
]
