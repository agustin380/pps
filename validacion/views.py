from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from validacion.models import Validacion, Pregunta
from validacion.forms import EncuestaForm


@login_required
def home(request):
    validacion = request.user.perfil.validacion
    return render(request, "validacion/resultado.html", {'estado': validacion.estado})


@login_required
def preguntas(request):
    validacion = request.user.perfil.validacion

    # Si la validacion ya termino redirigir al home
    if validacion.estado != Validacion.EN_CURSO:
        return redirect("home")

    preguntas = validacion.pregunta_set.filter(estado=Pregunta.SIN_RESPONDER)
    if not preguntas.exists():
        # Si se contestaron todas las preguntas calcular el resultado de la validacion
        validacion.calcular_resultado()
        return redirect("home")

    # Traer una pregunta y construirle opciones
    p = preguntas.first()
    opciones = p.construir_opciones()

    if request.method == "POST":
        form = EncuestaForm(request.POST, respuestas=opciones)
        if form.is_valid():
            resp = form.cleaned_data['respuesta']
            if resp == p.respuesta:
                p.correcta()
            else:
                p.incorrecta()
            return redirect("preguntas")
    else:
        # Si no vio la pregunta, marcar como vista
        # Si ya la vio, entonces dejo la encuesta sin responderla, marcar como incorrecta
        if p.visto:
            p.incorrecta()
            return redirect("preguntas")
        p.visto = True
        p.save()

    form = EncuestaForm(respuestas=opciones, initial={'respuesta': opciones[0]})

    return render(request, "validacion/preguntas.html", {'form': form, 'pregunta': p})
