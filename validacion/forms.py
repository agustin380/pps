from django import forms


class EncuestaForm(forms.Form):
    respuesta = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=[]
    )

    def __init__(self, *args, **kwargs):
        respuestas = kwargs.pop('respuestas', [])
        super().__init__(*args, **kwargs)

        if respuestas:
            resp = [(p, p.respuesta) for p in respuestas]
            self.fields['respuesta'].choices = resp
            self.fields['respuesta'].label = resp[0][0].get_tipo_display()
