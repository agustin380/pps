# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('validacion', '0003_auto_20150815_1944'),
    ]

    operations = [
        migrations.AddField(
            model_name='pregunta',
            name='visto',
            field=models.BooleanField(default=False),
        ),
    ]
