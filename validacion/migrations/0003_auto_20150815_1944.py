# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('validacion', '0002_auto_20150815_1850'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pregunta',
            name='respuesta',
            field=models.CharField(max_length=40, default='Ninguna de las anteriores'),
        ),
    ]
