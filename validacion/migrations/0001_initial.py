# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('dni', models.CharField(max_length=8)),
                ('usuario', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Pregunta',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('respuesta', models.CharField(max_length=40, default='Ninguna de las anteriores')),
                ('tipo', models.CharField(choices=[('d', '¿Reconocés alguno de estos domicilios, que pueden ser actuales o antiguos?'), ('e', '¿Trabajás o trabajaste para alguna de las siguientes empresas?'), ('v', '¿Tenés o has tenido en el pasado alguno de estos vehículos (automóvil / motocicleta)?')], max_length=1)),
                ('estado', models.CharField(choices=[('s', 'Sin responder'), ('c', 'Correcta'), ('i', 'Incorrecta')], max_length=1, default='s')),
            ],
        ),
        migrations.CreateModel(
            name='Validacion',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('estado', models.CharField(choices=[('r', 'Rechazada'), ('a', 'Aprobada'), ('m', 'Manual'), ('e', 'En curso')], max_length=1, default='e')),
                ('usuario', models.OneToOneField(blank=True, to='validacion.Perfil', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='pregunta',
            name='validacion',
            field=models.ForeignKey(null=True, to='validacion.Validacion'),
        ),
    ]
