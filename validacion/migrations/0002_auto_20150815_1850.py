# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('validacion', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pregunta',
            name='respuesta',
            field=models.CharField(default='Ninguna de las anteriores', blank=True, max_length=40),
        ),
    ]
