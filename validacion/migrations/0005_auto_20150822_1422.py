# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('validacion', '0004_pregunta_visto'),
    ]

    operations = [
        migrations.AddField(
            model_name='perfil',
            name='fecha_nacimiento',
            field=models.DateField(default=datetime.date(1990, 1, 1)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='perfil',
            name='telefono',
            field=models.CharField(default=2214562323, max_length=10),
            preserve_default=False,
        ),
    ]
