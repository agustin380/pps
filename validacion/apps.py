from django.apps import AppConfig


class ValidacionConfig(AppConfig):
    name = 'validacion'
    verbose_name = 'Aplicacion de Validacion'

    def ready(self):
        import validacion.signals
