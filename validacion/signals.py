from django.db.models.signals import post_save
from django.dispatch import receiver
from validacion.models import Perfil, Validacion, Pregunta


@receiver(post_save, sender=Perfil)
def crear_validacion(sender, instance, created, **kwargs):
    if created:
        Validacion.objects.create(usuario=instance)


@receiver(post_save, sender=Validacion)
def crear_preguntas(sender, instance, created, **kwargs):
    if created:
        Pregunta.objects.create(tipo=Pregunta.DOMICILIO, validacion=instance)
        Pregunta.objects.create(tipo=Pregunta.EMPRESA, validacion=instance)
        Pregunta.objects.create(tipo=Pregunta.VEHICULO, validacion=instance)
