from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib.admin.views.decorators import staff_member_required

from backoffice.forms import PreguntaFormSet
from validacion.models import Validacion

Usuario = get_user_model()


@staff_member_required
def usuarios(request):
    usuarios = Usuario.objects.filter(is_staff=False).order_by("-id")
    return render(request, "backoffice/usuarios.html", {'usuarios': usuarios})


@staff_member_required
def editar_encuesta(request, id):
    usuario = Usuario.objects.get(id=id)
    validacion = usuario.perfil.validacion
    preguntas = validacion.pregunta_set.all()
    forms = PreguntaFormSet(queryset=preguntas)
    if request.method == "POST":
        formset = PreguntaFormSet(request.POST, queryset=preguntas)
        if formset.is_valid():
            formset.save()
            return redirect("usuarios")

    ctx = {
        'forms': forms,
        'usuario': usuario
    }
    return render(request, "backoffice/encuesta.html", ctx)


@staff_member_required
def eliminar_encuesta(request, id):
    perfil = Usuario.objects.get(id=id).perfil
    perfil.validacion.delete()
    perfil.save()
    Validacion.objects.create(usuario=perfil)
    return redirect("usuarios")


@staff_member_required
def aprobar_validacion(request, id):
    validacion = Usuario.objects.get(id=id).perfil.validacion
    validacion.estado = Validacion.APROBADA
    validacion.save()
    return redirect("usuarios")


@staff_member_required
def rechazar_validacion(request, id):
    validacion = Usuario.objects.get(id=id).perfil.validacion
    validacion.estado = Validacion.RECHAZADA
    validacion.save()
    return redirect("usuarios")
