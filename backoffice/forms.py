from django import forms
from django.forms.models import modelformset_factory

from validacion.models import Pregunta


class PreguntaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['respuesta'].label = self.instance.get_tipo_display()

    class Meta:
        model = Pregunta
        fields = ['respuesta']

PreguntaFormSet = modelformset_factory(Pregunta, form=PreguntaForm, max_num=3)
