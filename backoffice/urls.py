from django.conf.urls import url
import backoffice.views as views


urlpatterns = [
    url(r'^usuarios/(?P<id>[0-9]+)/editar/$', views.editar_encuesta, name="editar_encuesta"),
    url(r'^usuarios/(?P<id>[0-9]+)/eliminar/$', views.eliminar_encuesta, name="eliminar_encuesta"),
    url(r'^usuarios/(?P<id>[0-9]+)/aprobar/$', views.aprobar_validacion, name="aprobar_validacion"),
    url(r'^usuarios/(?P<id>[0-9]+)/rechazar/$', views.rechazar_validacion, name="rechazar_validacion"),
    url(r'^usuarios/$', views.usuarios, name="usuarios"),
]
